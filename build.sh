#!/bin/bash
#
# Copyright © 2018-2019, "penglezos" <panagiotisegl@gmail.com>
# Thanks to Vipul Jha for zip creator
# Android Kernel Compilation Script
#

tput reset
echo -e "==============================================="
echo    "         Compiling Unknown Kernel             "
echo -e "==============================================="

LC_ALL=C date +%Y-%m-%d
date=`date +"%Y%m%d-%H%M"`
BUILD_START=$(date +"%s")
KERNEL_DIR=$PWD
REPACK_DIR=$KERNEL_DIR/AnyKernel3
OUT=$KERNEL_DIR/out
VERSION="r01"
export ARCH=arm64 && export SUBARCH=arm64
export CROSS_COMPILE="../aarch64-linux-android-5.x/bin/aarch64-linux-android-"
export TARGET_BUILD_VARIANT=user

#rm -rf out
#mkdir -p out
#make O=out clean
#make O=out mrproper
make O=out lotus_debug_defconfig
make O=out -j$(nproc --all) 

BUILD_END=$(date +"%s")
DIFF=$(($BUILD_END - $BUILD_START))
echo -e "Build completed in $(($DIFF / 60)) minute(s) and $(($DIFF % 60)) seconds."  
